import sys

from script import NewsParser

if __name__ == '__main__':
    url = sys.argv[1]
    new_parser = NewsParser(url)
    new_parser.run()
