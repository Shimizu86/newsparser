import os
from pathlib import Path
from urllib.parse import urlparse

import bs4
import requests
from parser_settings import get_tags_by_domain, char_limit


class NewsParser:
    def __init__(self, url):
        super(NewsParser, self).__init__()
        self.url = url

    def run(self):
        url_parser = UrlParser(self.url)
        parsed_url = url_parser.get_parsed_url()
        parsed_content = url_parser.parse_content()
        text_formatter = TextFormatter(
            parsed_content)
        formatted_text = text_formatter.format_text()
        FileWriter(parsed_url, formatted_text)


class UrlParser:
    """
    Принимает на вход ссылку на страницу.
    Получает код страницы.

    :param url: ссылка которую необходимо обработать
    :param domain: домен который необходимо обработать
    """

    def __init__(self, url, *args, **kwargs):
        super(UrlParser, self).__init__(*args, *kwargs)
        self.url = url
        self.domain = self.get_parsed_url().netloc

    def get_parsed_url(self):
        """
        Получает url с аргументами для дальнейшего использования
        :return: объект urlparse c аргументами
        """
        parsed_url = urlparse(self.url)
        return parsed_url

    def parse_content(self):
        """
        :return: возвращает BeautifulSoup объекты выбранные по заданным тегам
        """
        page = requests.get(self.url)
        page_content = bs4.BeautifulSoup(page.text, "html.parser")
        parsed_content = page_content.findAll(get_tags_by_domain(self.domain))
        return parsed_content


class TextFormatter:
    """
    Преобразует полученные от URLParser'а bs4 объекты к заданному формату

    :param text_to_format: BeautifulSoup объекты для обработки
    """

    def __init__(self, text_to_format, *args, **kwargs):
        super(TextFormatter, self).__init__(*args, **kwargs)
        self.text_to_format = text_to_format
        self.rewrite_links()

    def rewrite_links(self):
        """
        Заменяет теги со ссылками в полуенном коде  на квадратные скобки
        со ссылкой и последующим гипертекстом ссылки
        """
        for tag_content in self.text_to_format:
            for link in tag_content.find_all('a', href=True):
                link.replaceWith('[' + link['href'] + '] ' + link.text)

    def format_text(self):
        """
        Форматирует текст по заданным условиям (длина строки 80 символов,
        перенос по словам)
        :return Возвращает отформатированный текст
        """
        formatted_text = ''
        for content in self.text_to_format:
            n = 0
            text = content.text
            while n < len(text):
                if len(text) > char_limit:
                    n_next = n + char_limit if n + char_limit <= len(
                        text) else len(text)
                    if n_next != len(text):
                        while text[n_next - 1] != ' ':
                            n_next -= 1
                            if n_next == n + 1:
                                n_next = n + char_limit
                                break
                else:
                    n_next = len(text)
                formatted_text += text[n:n_next] + '\n'
                n = n_next
            formatted_text += '\n'
        return formatted_text


class FileWriter:
    """
    Получает аргументы ссылки и текст для записи в файл.
    Проверяет наличие пути для записи и при необходимости создает его
    Записывает данные в файл
    :param parsed_url: аргументы url'a для вычисления пути к будущему файлу
    :param text_to_write: форматированный текст для записи в файл
    """

    def __init__(self, parsed_url, text_to_write, *args, **kwargs):
        super(FileWriter, self).__init__(*args, **kwargs)
        self.parsed_url = parsed_url
        self.text_to_write = text_to_write
        self.path = self.create_path()
        self.write_to_file()

    def create_path(self):
        """
        Разбираем url (Убираем протокол "http://|https://" в начале, '/'
        или '.html|.shtml' в конце) и создаем абсолютный путь к файлу
        :return Возвращает абсоютный путь для сохранения файла
        """
        file_to_save_path = Path(os.getcwd()). \
            joinpath(self.parsed_url.netloc). \
            joinpath(self.parsed_url.path[1:]). \
            with_suffix('.txt')
        return file_to_save_path

    def write_to_file(self):
        """
        Записывает данные в файл
        """
        os.makedirs(os.path.dirname(self.path), exist_ok=True)
        with open(self.path, 'w+', encoding='utf-8') as file:
            file.write(self.text_to_write)
