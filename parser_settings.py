"""
Настройки для различных доменов.
Для более точного поиска по тегам с возможностью тонкой настройки
"""

char_limit = 80
sites = {
    'lenta.ru': {'h1': True, 'p': True, 'time': True},
    'gazeta.ru': {'h1': True, 'p': True, },
    'default': ['h1', 'h2', 'h3', 'p']}


def get_tags_by_domain(domain_name):
    """
    Получает домен в качестве аргумента и проверяет наличие настроек для него
    :param : domain_name: имя домена для проверки
    :return: возвращает список тегов для парсинга текста сайта
    """
    if domain_name in sites:
        return sites[domain_name]
    else:
        return sites['default']
