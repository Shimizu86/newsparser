import unittest
from unittest.mock import patch
from urllib.parse import ParseResult

import bs4
from script import UrlParser, TextFormatter


class TestUrlParser(unittest.TestCase):
    def test_get_parsed_url(self):
        url = 'https://docs.python.org/3.6/tutorial/appetite.html'
        example = ParseResult(scheme='https',
                              netloc='docs.python.org',
                              path='/3.6/tutorial/appetite.html',
                              params='', query='', fragment='')
        result = UrlParser(url).get_parsed_url()
        self.assertEqual(result, example)

    def test_parse_content(self):
        with patch('requests.get') as mock_request:
            url = 'http://google.com'
            mock_request.return_value.text = \
                "<p>Test</p><h1>Test 2</h1><div><span>Test3</span></div>"
            example_txt = '[<p>Test</p>, <h1>Test 2</h1>]'
            result = UrlParser(url).parse_content()
            self.assertEqual(str(result), example_txt)


class TestTextFormatter(unittest.TestCase):
    def test_rewrite_links(self):
        txt = '<p><a href="http://www.iana.org/example">One more...</a></p>'
        parsed_content = bs4.BeautifulSoup(txt, "html.parser")
        parsed_content = parsed_content.findAll(['h1', 'h2', 'h3', 'p'])
        example = '[<p>[http://www.iana.org/example] One more...</p>]'
        result = TextFormatter(parsed_content).text_to_format
        self.assertEqual(str(result), example)


if __name__ == '__main__':
    unittest.main()
